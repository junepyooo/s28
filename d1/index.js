//retrieved all posts following 
// the REST API (retrieve,/posts,GET)
fetch('https://jsonplaceholder.typicode.com/posts')
// use the json method from the "Reponse" object to convert
//the data into json FORMAT TO BE USED IN OUR APPLICATION 
.then (response => (response.json()))

//print the converted JSON value from the "fetch" request 
.then ((data) => console.log (data));


//POST 

fetch ('https://jsonplaceholder.typicode.com/posts', {
  method: 'POST',
  headers:{
    'Content-type':'application/json'
  },

  //sets the content/body data of the "request" object to be sent to the 
  //bckend. JSON.stringify converts the object data into a stringified JSON 

  body: JSON.stringify({
      title: 'New Post',
      body: 'Hello World',
      userID: 1
  })
})

.then ((response) => response.json())
.then((data) => console.log (data));

//Updating a post using PUT method  - put is for all properties 
fetch('https://jsonplaceholder.typicode.com/posts/1', {
    method: 'PUT', 
    headers: {
        'Content-type' : 'application/json'
    }, 

    body: JSON.stringify({
        id: 1, 
        title: "updated post", 
        body: "Hello Again",
        userID: 1

    })
})

.then ((response) => response.json())
.then((data) => console.log (data));


//USING PATCH  - patch is for specific property
fetch('https://jsonplaceholder.typicode.com/posts/1', {
    method: 'PATCH', 
    headers: {
        'Content-type' : 'application/json'
    },

    body: JSON.stringify({
        title: "Correct Title", 
    
    })
})

.then ((response) => response.json())
.then((data) => console.log (data));

// delete 
fetch('https://jsonplaceholder.typicode.com/posts/1', {
    method: 'DELETE'
})
.then ((response) => response.json())
.then((data) => console.log (data));

// retrieve a single method 

fetch('https://jsonplaceholder.typicode.com/posts/2')

.then ((response) => response.json())
.then((data) => console.log (data));

/*
CRUD 

C- Create (POST)
R - Read (GET)
U - Update (PUT/ PATCH )
D- Delete/ Destroy (delete)
*/