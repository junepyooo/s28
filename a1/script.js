// get all todos

fetch('https://jsonplaceholder.typicode.com/todos') 
.then (response => (response.json())) 
.then ((data) => console.log (data));

//getting a specific to do list item

fetch('https://jsonplaceholder.typicode.com/todos/2')

.then ((response) => response.json())
.then((data) => console.log (`The item "${data.title}"
has a status of ${data.completed}`));

// only the title will appear 

// data.map((todo)=>{
//     return todo.title;
// })

fetch('https://jsonplaceholder.typicode.com/todos') 
.then (response => (response.json())) 
.then((data) => {
    let list = data.map ((todo) =>{
        return todo.title
    })
    console.log (list)
})

//creating a to do list item using POST method 

//POST 

fetch ('https://jsonplaceholder.typicode.com/todos', {
  method: 'POST',
  headers:{
    'Content-type':'application/json'
  },
  body: JSON.stringify({
      title: 'Created a to do list item',
      completed: false,
      userID: 1
  })
})

.then ((response) => response.json())
.then((data) => console.log (data));


//PUT METHOD 

fetch('https://jsonplaceholder.typicode.com/todos/1', {
    method: 'PUT', 
    headers: {
        'Content-type' : 'application/json'
    }, 

    body: JSON.stringify({
        id: 1, 
        title: "updated to do list item",
        description: "To update the my to do list with a different data structure" ,
        status: "pending", 
        dateCompleted: "Pending", 
        userID: 1

    })
})

.then ((response) => response.json())
.then((data) => console.log (data));


// PATCH 
fetch('https://jsonplaceholder.typicode.com/todos/1', {
    method: 'PATCH', 
    headers: {
        'Content-type' : 'application/json'
    },

    body: JSON.stringify({
       status: "complete", 
       dateCompleted: "01/19/22"
    
    })
})

.then ((response) => response.json())
.then((data) => console.log (data));



//DELETE

fetch('https://jsonplaceholder.typicode.com/todos/1', {
    method: 'DELETE'
})
.then ((response) => response.json())
.then((data) => console.log (data));